package main

import "fmt"

func main() {

	// var data map[string]int
	// data["one"] = 1 // error

	// data2 := map[string]int{}
	// data2["pertama"] = 1
	// data2["kedua"] = 2
	// data2["ketiga"] = 3
	// data2["ketiga"] = 6

	// dataInt := map[int]string{}
	// dataInt[123] = "Seratus dua puluh tiga"
	// dataInt[124] = "Seratus dua puluh empat"

	// fmt.Println(dataInt)
	// fmt.Println(dataInt[122])

	// data3 := map[string]string{
	// 	"nama_depan":    "kinat",
	// 	"nama_belakang": "tria",
	// }

	// fmt.Println(data3)

	// // kombinasi slice dan map
	// var data = []map[string]string{
	// 	{"nama": "kinat", "gender": "pria", "color": "coklat"},
	// 	{"alamat": "bekasi timur", "id": "k001"},
	// 	{"jabatan": "golang developer"},
	// }

	// fmt.Println("Data asli : ", data)
	// fmt.Println("Data asli : ", data[2]["jabatan"])

	// fungsi dalam map
	data := make(map[string]string)
	data["nama"] = "kinat"
	data["alamat"] = "bekasi timur"
	data["jabatan"] = "golang developer"

	fmt.Println("Jumlah data map : ", len(data))
	fmt.Println("Semua data map : ", data)
	fmt.Println("Lihat data alamat : ", data["alamat"])

	data["jabatan"] = ""

	delete(data, "jabatan")

	fmt.Println("Semua data map after delete : ", data)

}
