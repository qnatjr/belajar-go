package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello World!")

	// // test go mod tidy
	// work := func() {
	// 	gobot.Every(1*time.Second, func() {
	// 		led.Toggle()
	// 	})
	// }
}
