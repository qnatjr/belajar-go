package main

import (
	"fmt"
	"strings"
)

func filter(data []string, callback func(string) bool) []string {
	var result []string // kosong
	for _, each := range data {
		if filtered := callback(each); filtered {
			result = append(result, each)
		}
	}
	return result
}

func main() {
	var data = []string{"kinat", "tria", "synatria"}

	var dataContainsO = filter(data, func(each string) bool {
		return strings.Contains(each, "a") // false or true
	})

	var dataLenght5 = filter(data, func(each string) bool {
		return len(each) == 5
	})

	fmt.Println(dataContainsO)
	fmt.Println(dataLenght5)
}
