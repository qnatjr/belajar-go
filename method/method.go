package main

import "fmt"

type person struct {
	Nama string
	Umur int
}

func (p person) sapa() {
	fmt.Println("Halo mas")
}

func (p person) sayHello() {
	fmt.Println("Halo! ", p.Nama)

}

func (p *person) gantiNama(nama string) {
	p.Nama = nama
}

func main() {

	// var data person
	// data.Nama = "fadel"
	// data.sayHello()

	// inisiasi awal
	dataAwal := person{
		Nama: "Kinat",
		Umur: 23,
	}
	fmt.Println(dataAwal)

	// panggil method
	dataAwal.sayHello()
	dataAwal.gantiNama("fadel")
	dataAwal.sayHello()
	fmt.Println(dataAwal)
}
