package main

import (
	"fmt"
)

func main() {

	// // input
	// var nama string

	// fmt.Print("Masukkan nama anda : ")
	// fmt.Scanln(&nama)
	// fmt.Println("Halo saya " + nama)

	// output
	fmt.Print("Halo nama saya kinat \n")
	fmt.Printf("Halo nama saya %s \n", "kinat")
	fmt.Println("Halo nama saya kinat")

	hasil := fmt.Sprint("Halo nama saya kinat \n")
	hasil2 := fmt.Sprintf("Ini angka %v \n", "tiga")
	hasil3 := fmt.Sprintln("Halo nama saya kinat")

	fmt.Println(hasil)
	fmt.Println(hasil2)
	fmt.Println(hasil3)

}
