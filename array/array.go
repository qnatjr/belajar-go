package main

import "fmt"

func main() {

	// inisiasi array dasar
	var names [3]string

	names[0] = "kinat"
	names[1] = "tria"
	names[2] = "subekti"
	names[3] = "synatria" // error

	// fmt.Println(names)

	// // inisiasi data awal
	// var hewan = [3]string{"kucing", "anjing", "ular"}
	// var hewan2 = [3]string{
	// 	"kucing",
	// 	"anjing",
	// 	"ular"}

	// fmt.Println(hewan, hewan2)

	// // inisiasi data awal tanpa menentukan jumlah elemen
	// var buah = [...]string{"apel", "mangga", "pisang", "apel", "mangga", "pisang"}
	// buah[2] = "nanas"
	// buah[5] = "sirsak"
	// fmt.Println(buah)
	// fmt.Println(len(buah))

	// array multidemensi
	var matrix = [2][3]int{{1, 2, 3}, {4, 5, 6}}
	var matrix2 = [3][3]int{{1, 2, 3}, {4, 5, 6}, {7, 8, 9}}

	fmt.Println(matrix)
	fmt.Println(matrix2)

}
