package main

import (
	"fmt"
)

func register(nama string, whitelist func(string) bool) {
	fmt.Println("sampai sini register")
	if whitelist(nama) {
		fmt.Println("Kamu ter whitelist oleh sistem")
	} else {
		fmt.Println("Kamu ter blacklist oleh sistem")

	}
}

func main() {
	fmt.Println("sampai sini main")

	whitelist := func(nama string) bool {
		fmt.Println("sampai sini whitelist")
		return nama == "kinat"
	}

	register("tria", whitelist)
	register("kinat", whitelist)

	fmt.Println("sampai sini selesai")

}
