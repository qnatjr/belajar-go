package main

import "fmt"

func main() {

	// // numerik
	// var angkaInt int = -922337203685477580
	// var angkaInt8 int8 = -127
	// var angkaInt16 int16 = 32767
	// var angkaInt32 int32 = 2147483647
	// var angkaInt64 int64 = 9223372036854775807
	// var angkaRune rune = 2147483647

	// fmt.Printf("Nilai int : %d \n", angkaInt)
	// fmt.Printf("Nilai int8 : %d \n", angkaInt8)
	// fmt.Printf("Nilai int16 : %d \n", angkaInt16)
	// fmt.Printf("Nilai int32 : %d \n", angkaInt32)
	// fmt.Printf("Nilai int64 : %d \n", angkaInt64)
	// fmt.Printf("Nilai rune : %d \n", angkaRune)

	// // desimal
	// var angkaFloat32 float32 = 2.9
	// var angkaFloat64 float64 = 0

	// fmt.Printf("Nilai float32 : %.3f \n", angkaFloat32)
	// fmt.Printf("Nilai float64 : %f \n", angkaFloat64)

	// // boolean
	// var dataTrue bool = true
	// var dataFalse bool = false

	// fmt.Printf("Nilai True : %t \n", dataTrue)
	// fmt.Printf("Nilai False : %t \n", dataFalse)

	// string
	var stringSingleLine string = "Hello World!"
	var stringMultiLine string = `Hello World!
	Saya sedang masak`

	fmt.Printf("Nilai Single Line : %s \n", stringSingleLine)
	fmt.Printf("Nilai Multi Line : %s \n", stringMultiLine)

}
