package main

import (
	"fmt"
	"strings"
)

func main() {
	// var data interface{}

	// data = "kinat"
	// fmt.Println("data")

	// data = []string{"kucing", "anjing", "ular"}
	// fmt.Println(data)

	// data = 10
	// fmt.Println(data)

	// // map interface
	// karyawan := map[string]interface{}{
	// 	"nama":     "kinat",
	// 	"umur":     10,
	// 	"keahlian": []string{"golang", "python"},
	// }
	// fmt.Println(karyawan)

	// fmt.Println("=====================================================")

	// // slice, map, interface
	// karyawanAll := []map[string]interface{}{
	// 	{"nama": "kinat", "umur": 10, "keahlian": []string{"golang", "python"}},
	// 	{"nama": "tria", "umur": 17, "keahlian": []string{"html", "css"}},
	// }

	// for _, v := range karyawanAll {
	// 	fmt.Println(v["nama"], "age is", v["umur"], "punya keahlian", v["keahlian"])
	// }

	// casting
	var value interface{}

	value = 5
	fmt.Println(value * 10)       // error
	fmt.Println(value.(int) * 10) // casting

	value = []string{"kucing", "anjing", "ular"}
	fmt.Println(value[0])                            // error
	var hewan = strings.Join(value.([]string), ", ") // casting
	fmt.Println(hewan[0])

}
