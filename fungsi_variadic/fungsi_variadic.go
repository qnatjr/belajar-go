package main

import "fmt"

func calculate(data ...int) {
	var hasil int
	for _, v := range data {
		hasil += v
	}

	fmt.Println("Hasil calculate : ", hasil)
}

func main() {
	calculate(1, 2, 3, 4, 5, 6, 7, 8, 9, 100)

	var data = []int{11, 22, 33, 44}
	calculate(data...)
}
