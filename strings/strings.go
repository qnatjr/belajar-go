package main

import (
	"fmt"
	"strconv"
	"strings"
)

func main() {

	nama := "SYNATRIA SUBEKTI"
	nomor := "08123456789"

	// result := strings.Contains(nama, "sny")
	// fmt.Println(result)

	result := strings.Count(nama, "b")
	fmt.Println(result)

	result2 := strings.HasPrefix(nomor, "0")
	result2 = strings.HasSuffix(nomor, "6789")
	fmt.Println(result2)

	// var dataNama string
	// dataNama = "Ichwan"

	var dataNama = []string{"Kinat", "Tria", "Subekti"}
	// dataNama2 := []string{"Kinat", "Tria", "Subekti"}
	// var dataNama3 []string = []string{"Kinat", "Tria", "Subekti"}

	// a := 3

	dataNamaBaru := strings.Join(dataNama, "-")
	fmt.Println(dataNama)
	// fmt.Println(dataNama2)
	// fmt.Println(dataNama3)
	fmt.Println(dataNamaBaru)

	dataAwal := strings.Split(dataNamaBaru, "-")
	fmt.Println(dataAwal)

	// dataNamaBaru = strings.Replace(dataNamaBaru, "i", "@", -1)
	dataNamaBaru = strings.ReplaceAll(dataNamaBaru, "i", "@")
	fmt.Println(dataNamaBaru)

	namaKecil := strings.ToLower(nama)
	fmt.Println(namaKecil)

	angka := "1"
	angkaSebenarnya, _ := strconv.Atoi(angka)
	fmt.Println(angkaSebenarnya)
	fmt.Printf("%T \n", angkaSebenarnya)

	angka = strconv.Itoa(2)
	fmt.Println(angka)
	fmt.Printf("%T", angka)

}
