package main

import "fmt"

func sayHello(nama string, umur int) (string, int) {
	hasil := "Halo " + nama
	umur = umur + 10
	return hasil, umur
}

func main() {

	nama := "kinat"
	hasil, umur := sayHello(nama, 23)

	fmt.Println(hasil)
	fmt.Println(umur)

}
