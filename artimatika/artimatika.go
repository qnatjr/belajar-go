package main

import "fmt"

func main() {

	// // operator biasa
	// var a = 8
	// var b = 3

	// fmt.Println("Hasil pejumlahan : ", a+b)
	// fmt.Println("Hasil pengurangan : ", a-b)
	// fmt.Println("Hasil perkalian : ", a*b)
	// fmt.Println("Hasil pembagian : ", a/b)
	// fmt.Println("Hasil sisa bagi : ", a%b)

	// // operator augmented assignment
	// var c = 10
	// c += 5 // c = c + 5

	// fmt.Println("Hasil : ", c)

	// // operator unary
	// var d = 10
	// d++
	// d++
	// d--
	// var e = false

	// fmt.Println("Hasil : ", d)
	// fmt.Println("Hasil kebalikan nilai e : ", !e)

	// // operator perbandingan
	// var nama1 = 8
	// var nama2 = 10
	// var hasil bool = nama1 > nama2

	// fmt.Println("Hasil perbandingan : ", hasil)

	// // operator logika / booelan
	// var baik = true
	// var jahat = false

	// var baikANDjahat bool = baik && jahat
	// fmt.Println("Baik AND Jahat : ", baikANDjahat)

	// var baikORjahat bool = baik || jahat
	// fmt.Println("Baik OR Jahat : ", baikORjahat)

	// var kebalikanBaik bool = !baik
	// fmt.Println("!Baik : ", kebalikanBaik)

	var r, luas, keliling float64

	fmt.Print("Masukkan panjang jari-jari lingkaran: ")
	fmt.Scanln(&r)
	luas = phi * r * r
	keliling = 2 * phi * r
	fmt.Println("Luas lingkaran adalah ", luas)
	fmt.Println("Keliling lingkaran adalah ", keliling)

}
