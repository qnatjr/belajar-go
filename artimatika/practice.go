package main

import "fmt"

func Lingkaran() {
	var r, luas, keliling, phi float64
	phi = 3.14
	fmt.Print("Masukkan panjang jari-jari lingkaran: ")
	fmt.Scanln(&r)
	luas = phi * r * r
	keliling = 2 * phi * r
	fmt.Println("Luas lingkaran adalah ", luas)
	fmt.Println("Keliling lingkaran adalah ", keliling)
}
