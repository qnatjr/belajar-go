package main

func main() {
	// // tipe penulisan
	// var firstname string // benar
	// var 1firstname string // salah
	// var !firstname string // salah
	// var first name string // salah

	// var lastName string // camelCase
	// var last_name string // snake_case
	// var LastName string // LastName

	// // manifest typing
	// var nama string
	// nama = "Kinat"

	// var umur int = 23

	// fmt.Printf("Nama saya %s dan umur saya %d", nama, umur)

	// // type inference
	// namaBaru := "Kinat"
	// umurBaru := 23

	// fmt.Printf("Nama saya %s dan umur saya %d", namaBaru, umurBaru)

	// // multi variable
	// var satu, dua, tiga int
	// satu, dua, tiga = 1, 2, 3

	// var A, B, C string = "A", "B", "C"

	// D, E, F := "D", "E", "F"

	// var (
	// 	pertama = "pertama"
	// 	kedua   = "kedua"
	// 	ketiga  = 3
	// )

	// fmt.Println(satu, dua, tiga, A, B, C, D, E, F, pertama, kedua, ketiga)

	// // underscore variable
	// _ = "Isi ga dipake"
	// _, value = 1, "pertama"

	// // konstanta
	// const nama string = "Kinat"
	// const umur = 23

	// nama = "Tria" // error
	// umur = 17     // error

	// const (
	// 	pertama = "pertama"
	// 	kedua   = 2
	// )

}
