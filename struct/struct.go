package main

import "fmt"

type datalain struct {
	alamat string
}

type person struct {
	// datalain
	nama  string
	age   int
	kelas int
}

type NestedStruct struct {
	person struct {
		name string `json:"name"`
		age  int    `json:"age"`
	} `json:"person"`
	grade   int      `json:"grade"`
	hobbies []string `json:"hobbies"`
}

func main() {
	var p1 person
	p1.nama = "fadel"
	p1.age = 17
	p1.kelas = 10
	p1.datalain.alamat = "bekasi"
	// // p1.nama = "kinat"
	// // p1.age = 23

	// fmt.Println("p1 :", p1)

	// var p2 = person{}
	// p2.nama = "tria"
	// p2.age = 23

	// fmt.Println("p2 :", p2)

	// var p3 = person{
	// 	age:  23,
	// 	nama: "synatria",
	// }

	// fmt.Println(p3)

	// var structBaru = struct {
	// 	alamat  string
	// 	jabatan string
	// }{
	// 	alamat:  "Bekasi TImur",
	// 	jabatan: "go dev",
	// }

	// fmt.Println("Anonymous Struct :", structBaru)
	// fmt.Println("Anonymous Struct alamat :", structBaru.alamat)

	// anonymous struct tanpa pengisian property
	// var s1 = struct {
	// 	person
	// 	class int
	// }{}

	// anonymous struct dengan pengisian property
	// var s2 = struct {
	// 	person
	// 	class int
	// }{
	// 	person: person{"kinat", 23, 10},
	// 	class:  2,
	// }

	// fmt.Println(s2)

	// slice & struct
	var allPersons = []person{
		{nama: "kinat", age: 23, kelas: 10},
		{nama: "tria", age: 23},
		{nama: "synatria", age: 22},
	}

	// var orangBaru []person
	// orangBaru[0] = person{nama: "Fadel", age: 33}
	// orangBaru[1] = person{nama: "kinat", age: 17}

	fmt.Println(allPersons)
	fmt.Println(allPersons[0])
	fmt.Println("--------------------")

	for _, person := range allPersons {
		fmt.Println(person.nama, "age is", person.age)
	}

}
