package main

import "fmt"

func fungsiMultipleParameter(nama1, nama2, nama3 string) {
	fmt.Println(nama1)
	fmt.Println(nama2)
	fmt.Println(nama3)
}

func pembagian(angka1, angka2 int) {
	if angka2 == 0 {
		fmt.Println("Angka tidak boleh 0")
		return
	}

	fmt.Println("Hasil : ", angka1/angka2)
}
func main2() {

	// // parameter bertipe data sama
	// fungsiMultipleParameter("kinat", "kinat", "tria")

	// force exit with return
	pembagian(10, 0)
}

func main() {

	main2()
}
