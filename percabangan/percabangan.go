package main

import "fmt"

func main() {

	// nama := "kinat"
	// ganteng := false

	// if nama == "kinat tria" {
	// 	fmt.Println("Halo ", nama)
	// } else {
	// 	fmt.Println("Halo orang lain")
	// }

	// if ganteng {
	// 	fmt.Println("Kamu ganteng!")
	// } else {
	// 	fmt.Println("Kamu b aja!")
	// }

	// // else if
	// if nama == "kinat" {
	// 	fmt.Println("Halo kinat")
	// } else if nama == "tria" {
	// 	fmt.Println("Halo tria")
	// } else {
	// 	fmt.Println("Kamu siapa ?")
	// }

	// // if short statment
	// if length := len(nama); length < 10 {
	// 	fmt.Println("Nama terlalu pendek ")
	// } else {
	// 	fmt.Println("Nama mantap!")
	// }

	// switch
	umur := 23

	switch umur {
	case 23:
		fmt.Println("Kamu 23 tahun")
	case 20:
		fmt.Println("Kamu 20 tahun")
	default:
		fmt.Println("Kamu umur berapa ?")
	}

	// switch tanpa kondisi
	umur = 100

	switch {
	case umur < 17:
		fmt.Println("Kamu anak-anak")
	case umur > 17 && umur < 30:
		fmt.Println("Kamu remaja")
	default:
		fmt.Println("Kamu dewasa")
	}

}
