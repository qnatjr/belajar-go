package main

import "fmt"

func main() {

	// break : memberhentikan perulangan
	// continue : next iterasi

	// // perulangan dengan iterasi
	// for i := 0; i < 10; i++ {
	// 	if i == 5 {
	// 		continue
	// 	}
	// 	fmt.Println("Angka : ", i)
	// }

	// // perulangan dengan kondisi
	// a := 0
	// for a < 5 {
	// 	fmt.Println("Perulangan : ", a)
	// 	// a++
	// }

	// for - range
	// dataBuah := []string{"apel", "mangga", "pisang"}
	// dataManusia := map[string]string{
	// 	"nama":    "kinat",
	// 	"alamat":  "bekasi timur",
	// 	"jabatan": "golang developer",
	// }

	// for i, v := range dataBuah {
	// 	fmt.Println("Index Buah ke", i+1, "adalah", v)
	// }

	// for i := 0; i < 3; i++ {
	// 	dataBuah = append(dataBuah, "sirsak")
	// }

	// fmt.Println(dataBuah)

	// for k, v := range dataManusia {
	// 	fmt.Println(k + " = " + v)
	// }

	// for tak terbatas
	var i = 0
	for {
		if i%2 == 0 {
			i++
			continue
		}

		// if i == 10 {
		// 	break
		// }

		fmt.Println("Angka : ", i)
		i++
	}
}
