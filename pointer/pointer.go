package main

import "fmt"

func main() {

	var angka int = 4
	var angkaBaru *int = &angka

	angka = 7

	fmt.Println("angka (value)   :", angka) // 4
	fmt.Println("angka (address) :", &angka)

	fmt.Println("angkaBaru (value)   :", *angkaBaru) // 4
	fmt.Println("angkaBaru (address) :", angkaBaru)

}
