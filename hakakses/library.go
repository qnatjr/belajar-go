package hakakses

import "fmt"

func HelloWorld() {
	fmt.Println("Hello World")
}

func perkenalan(nama string) {
	fmt.Println("Perkenalkan saya", nama)
}
