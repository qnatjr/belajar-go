package main

import "fmt"

func persegi(panjang int, lebar int) (int, int) {
	luas := panjang * lebar
	keliling := panjang * 4

	return luas, keliling
}

func main() {
	var luas, keliling int
	luas, keliling = persegi(10, 5)

	fmt.Println("luas : ", luas)
	fmt.Println("keliling : ", keliling)

}
