package main

import "fmt"

func main() {

	// // perbedaan array dan slice
	// var buahA = []string{"apple", "grape"}    // slice
	// var buahB = [2]string{"apple", "grape"}   // array
	// var buahC = [...]string{"apple", "grape"} // array

	// // slicing from array
	// var dataBuah = []string{"apel", "mangga", "pisang", "nanas"} // data slice

	// fmt.Println(dataBuah[1:2])
	// fmt.Println(dataBuah[1:])
	// fmt.Println(dataBuah[:2])
	// fmt.Println(dataBuah[:])
	// fmt.Println(dataBuah)

	// fungsi dalam slice
	var dataOrang = []string{"kinat", "tria"}
	var dataHewan = make([]string, 3, 3)

	dataHewan[0] = "kucing"
	dataHewan[1] = "anjing"
	dataHewan[2] = "ular"

	fmt.Println("Data Orang : ", dataOrang)
	fmt.Println("Jumlah Orang : ", len(dataOrang))
	fmt.Println("Kapasitas Orang : ", cap(dataOrang))

	dataOrang = append(dataOrang, "synatria")

	fmt.Println("Data Orang : ", dataOrang)
	fmt.Println("Jumlah Orang : ", len(dataOrang))
	fmt.Println("Kapasitas Orang : ", cap(dataOrang))

	dataOrang = append(dataOrang, "synatria")

	fmt.Println("Data Orang : ", dataOrang)
	fmt.Println("Jumlah Orang : ", len(dataOrang))
	fmt.Println("Kapasitas Orang : ", cap(dataOrang))

	dataOrang = append(dataOrang, "synatria")

	fmt.Println("Data Orang : ", dataOrang)
	fmt.Println("Jumlah Orang : ", len(dataOrang))
	fmt.Println("Kapasitas Orang : ", cap(dataOrang))

}
