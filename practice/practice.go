package main

import "fmt"

func main() {
	// // practice 1
	// var r float64
	// r = 7.0

	// luas := 3.14 * r * r
	// keliling := 2 * 3.14 * r

	// fmt.Println("Luas :", luas)
	// fmt.Println("Keliling :", keliling)

	// // practice 2
	// var (
	// 	ujianHarian1 float64 = 20
	// 	ujianHarian2 float64 = 81
	// 	ujianAkhir   float64 = 30
	// )

	// threshold := (ujianHarian1 + ujianHarian2 + ujianAkhir) / 3

	// if threshold >= 75 {
	// 	fmt.Println("Selamat! Kamu lulus")
	// } else {
	// 	fmt.Println("Maaf! Kamu belum lulus")
	// }

	// // practice 3

	var data = []int{32, 53, 643, 45, 12, 451}

	for i, v := range data {
		fmt.Println(data[i])
		hasil := v % 2
		if hasil == 0 {
			fmt.Println("Nilai : ", v, "Genap !")
		} else {
			fmt.Println("Nilai : ", v, "Ganjil !")
		}
	}
}
