package main

import (
	"fmt"
	"math"
)

type hitung interface {
	luas() float64
	keliling() float64
}

type lingkaran struct {
	diameter float64
}

func (l lingkaran) jarijari() float64 {
	return l.diameter / 2
}

func (l lingkaran) luas() float64 {
	return math.Pi * math.Pow(l.jarijari(), 2)
}

func (l lingkaran) keliling() float64 {
	return math.Pi * l.diameter
}

// type persegiPanjang struct {
// 	panjang float64
// 	lebar   float64
// }

// func (p persegiPanjang) luas() float64 {
// 	return p.panjang * p.lebar
// }

// func (p persegiPanjang) keliling() float64 {
// 	return 2 * (p.panjang * p.lebar)
// }

func main() {
	var bangunDatar hitung // pemanggilan interface kontrakan

	bangunDatar = persegiPanjang{panjang: 10.0, lebar: 5.0}

	fmt.Println("========== Persegi panjang ==========")
	fmt.Println("Luas :", bangunDatar.luas())
	fmt.Println("Keliling :", bangunDatar.keliling())

	var data = lingkaran{diameter: 7}
	fmt.Println(data.jarijari())

	bangunDatar = lingkaran{14.0}

	fmt.Println("========== lingkaran ==========")
	fmt.Println("Luas :", bangunDatar.luas())
	fmt.Println("Keliling :", bangunDatar.keliling())
	fmt.Println("Jari Jari :", bangunDatar.(lingkaran).jarijari()) // casting
}
